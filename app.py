from flask import Flask, render_template, request, redirect
from deploy_commands.bacc import get_bacc

app = Flask(__name__)


@app.route("/")
def hello():
    return render_template('home.html')


@app.route("/report-issues")
def report_issues():
    return redirect("https://www.youtube.com/watch?v=dQw4w9WgXcQ", code=302)


@app.route("/bacc", methods=['GET'])
def bacc():
    return render_template('bacc.html')


@app.route("/bacc", methods=['POST'])
def bacc_post():
    id = request.form['id'].strip()
    res = get_bacc(id)
    return render_template(
        'bacc_done.html',
        id=id,
        deploy=res.get('deploy'),
        revert=res.get('revert'),
        error=res.get('error'),
        warning=res.get('warning'))


@app.route("/bacc/<id>", methods=['GET'])
def bacc_get(id):
    res = get_bacc(id)
    return render_template(
        'bacc_done.html',
        id=id,
        deploy=res.get('deploy'),
        revert=res.get('revert'),
        error=res.get('error'),
        warning=res.get('warning')
    )


if __name__ == "__main__":
    app.run(host='0.0.0.0')
